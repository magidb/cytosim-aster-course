# /usr/bin/python3
####### PACKAGES

from cytolysis.objects import Object_set,Object
import numpy as np


class Aster_set(Object_set):

    """ Aster_set is a set of aster
        Aster is a solid, of which the position, force, and linker forces may be reported
        """

    def read_objects(self, *args, reports=None, **kwargs):
        DIM = self.dim
        keys = reports.keys()

        try:
            fiber_rows = None   
            fiber_forces = None
            force = None

            if "force" in keys:
                rows = np.loadtxt(reports['force'], comments="%")
            elif "position" in keys:
                rows = np.loadtxt(reports['position'], comments="%")
            if "link" in keys:
                fiber_rows = np.loadtxt(reports['link'], comments="%")
                # print(fiber_rows)
            if len(rows.shape) == 1:
                rows = [rows]
            for row in rows:
                id = row[1]
                position = row[2:2 + DIM]
                if "force" in keys:
                    force = row[2 + DIM:2 + DIM + DIM]
                if fiber_rows is not None:
                    fiber_forces = fiber_rows[fiber_rows[:, 0] == id, 1:]
                self.append(Aster(position=position, force=force, id=id,
                                       fiber_forces=fiber_forces))
        except:
            print("Could not read Aster reports")


class Aster(Object):
    """ Aster
        Generic object with id and position
        plus a force, plus posssibly fiber forces

        Fiber_forces is an numpy array containing :
            Fiber_ID, Linker_position, Force, Torque
        """

    def __init__(self, *args, force=None, fiber_forces=None, **kwargs):
        self.force = force
        self.fiber_forces = fiber_forces
        Object.__init__(self, *args, **kwargs)